import asyncio
import json
import math
import random
import sys

import websockets

SNAKE_RADII = 16
DISTANCE = 10


async def hello(ip):
    async with websockets.connect(f'ws://{ip}:6789') as websocket:
        await websocket.send(json.dumps({
            'event': 'new_client',
            'data': 'player',
        }))
        await websocket.send(json.dumps({
            'event': 'player_joined',
            'data': 'test_snake' + str(random.randint(0, 100)),
        }))
        message = await websocket.recv()
        async for message in websocket:
            message = json.loads(message)
            if message['event'] == 'game_over':
                return

            data = message['data']
            player_id = data['player_id']
            try:
                [snake] = (snake for snake in data['players'] if snake['id'] == player_id)
            except ValueError:
                continue

            snake = snake['snake']
            await websocket.send(json.dumps({
                'event': 'player_direction',
                'data': {'direction': find_safe_angle(snake[0], data['players'], closest(snake[0], data['fruits']))},
            }))


def closest(point, points):
    def distance(p):
        return (
            ((p['x'] - point['x']) ** 2)
            + ((p['y'] - point['y']) ** 2)
        )

    return sorted(points, key=distance)


def another_snake_ahead(head, angle, snakes):
    ahead = {
        'x': DISTANCE * 10 * math.cos(angle) + head['x'],
        'y': DISTANCE * 10 * math.sin(angle) + head['y'],
    }
    for snake in snakes:
        for node in snake:
            if node == head:
                break

            if distance(node, ahead) < DISTANCE * 5 + SNAKE_RADII:
                return True

    return False


def find_safe_angle(head, players, fruits):
    if not fruits:
        return 0

    p = fruits[0]
    angle = math.atan2(p['y'] - head['y'], p['x'] - head['x'])
    if not another_snake_ahead(head, angle, (p['snake'] for p in players)):
        return angle

    return find_safe_angle(head, players, fruits[1:])


def distance(p1, p2):
    return math.sqrt(((p1['x'] - p2['x']) ** 2
                     + (p1['y'] - p2['y']) ** 2))


async def main():
    bots_count = int(sys.argv[1]) if len(sys.argv) > 1 else 10
    ip = sys.argv[2] if len(sys.argv) > 2 else '127.0.0.1'
    await asyncio.gather(*(hello(ip) for _ in range(bots_count)))

asyncio.run(main())
