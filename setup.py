import setuptools

setuptools.setup(
    name="snakes_server",
    version="0.1.0",
    author="Auxiliary Teams",
    author_email="author@example.com",
    description='Game server for the "Snakes" slither.io clone.',
    packages=setuptools.find_packages("src"),
    package_dir={"": "src"},
    entry_points={
        "console_scripts": ["snakes_server=snakes_server.main:main"]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'websockets',
    ],
)
