import collections
import datetime
import itertools
import math
import random
import time
import operator

DEFAULT_ACTIONS = {
    'direction': None,
    'is_fast': None,
}

FIELD_LIMITS = {
    'x': 2000,
    'y': 2000,
}

OBJECT_RADII = {
    'snake': 16,
    'fruit': 6,
}

MIN_FRUITS = 100


class World:
    def __init__(self):
        self.players = dict()
        self.fruits = list()
        self.best_scores = list()
        self.stats = {
            'by_hour': [[0, 0]] * 24,
            'by_day': [[0, 0]] * 7,
            'avg_time': [0, 0],
            'max_score': 0,
        }
        for _ in range(100):
            self.fruits.append(Fruit())

    def add_player(self, player, name):
        self.players[player] = Player(hash(player), name)

    def delete_player(self, player):
        ply = self.players[player]
        name = ply.name
        score = ply.points
        time_played = time.time() - ply.connection_time
        self.best_scores.append((name, score, time_played))
        getscore = operator.itemgetter(1)
        self.best_scores = sorted(self.best_scores, key=getscore, reverse=True)
        self.best_scores = self.best_scores[:10]

        avg, num = self.stats['avg_time']
        self.stats['avg_time'] = (new_average(avg, num + 1, time_played), num + 1)
        self.stats['max_score'] = self.best_scores[0][1]

        del self.players[player]

    def get_players(self):
        return self.players

    def tick(self):
        curr_hour = datetime.datetime.now().hour
        avg, num = self.stats['by_hour'][curr_hour]
        self.stats['by_hour'][curr_hour] = ((new_average(avg, num + 1, len(self.players))), num + 1)

        curr_day = datetime.datetime.now().weekday()
        avg, num = self.stats['by_day'][curr_day]
        self.stats['by_day'][curr_day] = ((new_average(avg, num + 1, len(self.players))), num + 1)

        for players in itertools.permutations(self.get_players().items(), 2):
            first, second = players
            first_id, first_snake = first
            _, second_snake = second
            if self.is_first_snake_head_inside_second_snake(first_snake, second_snake):
                for _ in range(int(first_snake.points) // 2):
                    f = Fruit()
                    head = next(first_snake.get_snake_as_coordinates())
                    f.move_to_around_point(head)
                    self.fruits.append(f)

                self.delete_player(first_id)
                break

        for player in self.get_players().values():
            for fruit in self.fruits.copy():
                if self.is_player_on_fruit(player, fruit):
                    if len(self.fruits) > MIN_FRUITS:
                        self.fruits.remove(fruit)
                    else:
                        fruit.move_to_random_location()

                    player.grow_snake()

            player.move_snake()

    def is_player_on_fruit(self, player, fruit):
        snake = next(player.get_snake_as_coordinates())
        radii_sub = (OBJECT_RADII['snake'] - OBJECT_RADII['fruit']) ** 2
        radii_sum = (OBJECT_RADII['snake'] + OBJECT_RADII['fruit']) ** 2
        pos_sum = ((snake['x'] - fruit.x) ** 2) + ((snake['y'] - fruit.y) ** 2)

        return radii_sub <= pos_sum <= radii_sum

    def is_first_snake_head_inside_second_snake(self, first_player, second_player):
        first_snake_head = next(first_player.get_snake_as_coordinates())
        for second_snake_chunk in second_player.get_snake_as_coordinates():
            radii_sum = (OBJECT_RADII['snake'] << 1) ** 2
            pos_sum = ((first_snake_head['x'] - second_snake_chunk['x']) ** 2
                       + (first_snake_head['y'] - second_snake_chunk['y']) ** 2)
            if 0 <= pos_sum <= radii_sum:
                return True

        return False

    def serialize(self):
        players = [player.serialize() for player in self.get_players().values()]
        fruits = [{'x': fruit.x, 'y': fruit.y} for fruit in self.fruits]
        stats = self.stats
        stats = {
            'by_hour': [elem[0] for elem in stats['by_hour']],
            'by_day': [elem[0] for elem in stats['by_day']],
            'avg_time': stats['avg_time'][0],
            'max_score': stats['max_score'],
        }

        return (players, fruits, self.best_scores, stats)


class Fruit:
    def __init__(self):
        self.move_to_random_location()

    def move_to_random_location(self):
        self.x = random.randint(-FIELD_LIMITS['x'], FIELD_LIMITS['x'])
        self.y = random.randint(-FIELD_LIMITS['y'], FIELD_LIMITS['y'])

    def move_to_around_point(self, point):
        radius = OBJECT_RADII['snake'] * 10
        self.x = random.randint(point['x'] - radius, point['x'] + radius)
        while self.x > FIELD_LIMITS['x'] or self.x < -FIELD_LIMITS['x']:
            self.x = random.randint(point['x'] - radius, point['x'] + radius)

        self.y = random.randint(point['y'] - radius, point['y'] + radius)
        while self.y > FIELD_LIMITS['y'] or self.y < -FIELD_LIMITS['y']:
            self.y = random.randint(point['y'] - radius, point['y'] + radius)


class Player:
    def __init__(self, id_hash, name):
        self.actions = DEFAULT_ACTIONS.copy()
        self.id = id_hash
        self.name = name
        self.current_angle_rad = 0
        self.points = 10
        self.snake = collections.deque(
            ((random.randint(-FIELD_LIMITS['x'], FIELD_LIMITS['x']),
             random.randint(-FIELD_LIMITS['y'], FIELD_LIMITS['y'])),)
            * self.points
        )
        self.connection_time = time.time()

    def set_actions(self, actions):
        self.actions = actions

    def get_actions(self):
        return self.actions

    def move_snake(self):
        if self.actions['direction'] is None:
            self.actions['direction'] = 0

        max_rotation_rad = 0.2
        distance = 10

        new_angle_rad = self.actions['direction']
        current_new_delta = math.pi - abs(abs(self.current_angle_rad - new_angle_rad) - math.pi)
        if current_new_delta < max_rotation_rad:
            turn_angle_rad = new_angle_rad
        else:
            new_minus_current = new_angle_rad - self.current_angle_rad
            if new_minus_current < 0:
                new_minus_current += 2 * math.pi

            if new_minus_current > math.pi:
                turn_angle_rad = self.current_angle_rad - max_rotation_rad
            else:
                turn_angle_rad = self.current_angle_rad + max_rotation_rad

            turn_angle_rad = math.atan2(math.sin(turn_angle_rad), math.cos(turn_angle_rad))

        x1, y1 = self.snake[0]
        x2 = x1 + distance * math.cos(turn_angle_rad)
        y2 = y1 + distance * math.sin(turn_angle_rad)
        if abs(x2) > FIELD_LIMITS['x']:
            x2 = -math.copysign(FIELD_LIMITS['x'], x2)
        if abs(y2) > FIELD_LIMITS['y']:
            y2 = -math.copysign(FIELD_LIMITS['y'], y2)

        self.snake.appendleft((int(x2), int(y2)))
        self.snake.pop()
        self.current_angle_rad = turn_angle_rad

    def grow_snake(self):
        x, y = self.snake[-1]
        self.snake.append((x, y))
        self.points += 1

    def chop_snake(self):
        self.snake.pop()

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'snake': tuple(self.get_snake_as_coordinates()),
            'direction': self.actions['direction'],
            'points': self.points,
        }

    def get_snake_as_coordinates(self):
        for node in self.snake:
            yield {'x': node[0], 'y': node[1]}


def new_average(avg, n, new_sample):
    avg -= avg / n
    avg += new_sample / n
    return avg
