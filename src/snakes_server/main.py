import asyncio
import json

import websockets

import snakes_server.game

world = snakes_server.game.World()
connected = {k: set() for k in ('players', 'guests', 'admins')}


async def handle_new_client(websocket, path):
    message = await websocket.recv()
    message = json.loads(message)
    if message['event'] != 'new_client':
        return

    # TODO: use enum.
    if message['data'] == 'player':
        await update_player_actions(websocket)
    elif message['data'] == 'guest':
        await wait_on_guest(websocket)
    elif message['data'] == 'admin':
        await wait_on_admins(websocket)


async def send_world(tickrate_secs, target_clients: set):
    while True:
        players, fruits, best_scores, _ = world.serialize()
        messages = [ws.send(json.dumps({
            'event': 'tick',
            'data': {
                'players': players,
                'fruits': fruits,
                'best_scores': best_scores,
                'player_id': hash(ws),
            }
        })) for ws in target_clients]
        if messages:
            await asyncio.wait(messages)
        await asyncio.sleep(tickrate_secs)


async def recalculate_world(tickrate_secs):
    while True:
        world.tick()
        await asyncio.sleep(tickrate_secs)


async def update_player_actions(websocket):
    message = await websocket.recv()
    message = json.loads(message)
    if message['event'] != 'player_joined':
        return
    player_name = message['data']
    connected['players'].add(websocket)
    world.add_player(websocket, player_name)
    await websocket.send(json.dumps({
        'event': 'field_limit',
        'data': snakes_server.game.FIELD_LIMITS['x'],
    }))
    try:
        async for message in websocket:
            message = json.loads(message)
            players = world.get_players()
            if message['event'] == 'player_direction':
                if websocket in players:
                    players[websocket].set_actions({k: message['data'].get(k) for k
                                                    in snakes_server.game.DEFAULT_ACTIONS.keys()})

            if websocket not in players:
                await websocket.send(json.dumps({
                    'event': 'game_over',
                }))
                break
    finally:
        if websocket in world.get_players():
            world.delete_player(websocket)

        connected['players'].remove(websocket)


async def wait_on_guest(websocket):
    connected['guests'].add(websocket)
    _, _, _, stats = world.serialize()
    await websocket.send(json.dumps({
        'event': 'stats',
        'data': stats,
    }))
    try:
        async for message in websocket:
            print(message)
    finally:
        connected['guests'].remove(websocket)


async def wait_on_admins(websocket):
    connected['admins'].add(websocket)
    await websocket.send(json.dumps({
        'event': 'field_limit',
        'data': snakes_server.game.FIELD_LIMITS['x'],
    }))
    try:
        async for message in websocket:
            print(message)
    finally:
        connected['admins'].remove(websocket)


def main():
    tasks = (
        websockets.serve(handle_new_client, '0.0.0.0', 6789),
        send_world(0.02, connected['players']),
        send_world(1, connected['guests']),
        send_world(0.02, connected['admins']),
        recalculate_world(0.02),
    )
    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(asyncio.wait(tasks))
    event_loop.run_forever()
